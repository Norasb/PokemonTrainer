import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.css']
})

export class ButtonComponent {
    @Input('buttonText') buttonTxt = '';

    @Output() buttonAction: EventEmitter<any> = new EventEmitter();
    
    buttonClicked() {
        this.buttonAction.emit();
    }
}