# PokemonTrainer

## About
This site was made as an assignment submission to display competanse in angular front-end.

The site lets users register and login to access a catalogue of pokemons from which they can capture pokemons to display in their trainer pages.

If you dont like your pokemons you can also release it into the wild.

## How to Use the website
run `npm install` in order to make sure all dependencies are installed. 

then simply run `npm start` or `ng serve` to launch the site, it will launch by default on localhost:4200/ 

## Contributors
This project was made as a team effort from Tobias Søyland, Nora sophie Backe and Eirik Torp.

### Go catch 'em all.✊