export interface PokemonTrainer{
    id: number;
    username: string;
    pokemon: string[];
}