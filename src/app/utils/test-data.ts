import { Pokemon } from "../models/pokemon";
import { PokemonTrainer } from "../models/pokemon-trainer.model";

// test data used for devlopment. main purpose was to prevent fuckups in the api.

export function getTrainers():PokemonTrainer[] {
    let trainerData = [
        {
            "id": 1,
            "username": "ash",
            "pokemon": [
              "bulbasaur",
              "pikachu"
            ]
        },
        {
            "id": 2,
            "username": "gary",
            "pokemon": [
              "squirtle",
              "golem"
            ]
        },
        {
            "id": 3,
            "username": "misty",
            "pokemon": [
              "psyduck",
              "togepi"
            ]
        },
        {
            "id": 4,
            "username": "jessie",
            "pokemon": [
              "meowth",
              "ekans"
            ]
        }
    ]
    return trainerData
}

export function getPokemons():Pokemon[]{
  let pokemonData = [
    {
      "id": 35,
      "name": "clefairy",
      "height": 6,
      "weight": 75,
      "sprites": [
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-viii/icons/35.png",
      ],
      "types": ["fairy"]
    }
  ]
  return pokemonData
}