export const environment = {
    production: true,
    apiUsers: "https://statuesque-complex-carrot.glitch.me/trainers",
    pokeApiUrl: "https://pokeapi.co/api/v2/pokemon?limit=905",
    basePokeApiUrl : "https://pokeapi.co/api/v2/pokemon/"
};