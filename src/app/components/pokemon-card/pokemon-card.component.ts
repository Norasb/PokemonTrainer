import { Component, Input } from "@angular/core";
import { CataloguePokemon } from "src/app/models/pokemon";
import { PokemonTrainerService } from "src/app/services/pokemon-trainer.service";
import { PokemonService } from "src/app/services/pokemon.service";
import { environment } from "src/environments/environment.development";

const { basePokeApiUrl } = environment;

@Component({
    selector: 'app-pokemon-card',
    templateUrl: './pokemon-card.component.html',
    styleUrls: ['./pokemon-card.component.css']
})

export class PokemonCard{

    //Makes Service available
    constructor(
        private readonly pokemonService: PokemonService,
        private readonly pokemonTrainerService : PokemonTrainerService) {}

    //Uses Service to stores pokemon in state and in in API
    //Changes local property "caught" so that pokeball is displayed in Card
    public onCatchClicked(): void {
        this.currentPokemon.caught = true
        this.pokemonService.addPokemon(basePokeApiUrl+this.currentPokemon.name)
        this.pokemonTrainerService.addToPokemons(this.currentPokemon.name)
    }

    //Initializes an instance of CataloguePokemon that will be changed by the parent component
    @Input() currentPokemon:CataloguePokemon = new CataloguePokemon(0, "", false);


}