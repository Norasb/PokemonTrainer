import { Component, OnInit } from "@angular/core";
import { CataloguePokemon, pokemonList } from "src/app/models/pokemon";
import { PokemonService } from "src/app/services/pokemon.service";

@Component({
    selector: 'app-catalogue-listing',
    templateUrl: './catalogue-listing.component.html',
    styleUrls: ['./catalogue-listing.component.css']
})

export class CatalogueListing implements OnInit {
    
    //Makes Service available
    constructor(
        private readonly pokemonService: PokemonService) {}
        
    //Initialize list that will be filled by OnInit
    public pokemonCatalogue:CataloguePokemon[] = [];

    //returns pokemon id from url
    public getIdByUrl = (url: string):number => {
        return parseInt(url.split('/')[6])
    }

    //Checks global state if pokemon is already caught
    public isPokemonCaught = (name: string): boolean => {
        let caughtPokemons = this.pokemonService.getPokeCollection()
        for (let pokemon of caughtPokemons) {
            if (pokemon.name == name) {
                return true
            }
        }
        return false
    }

    async ngOnInit(): Promise<void> {
        
        //Reads list of all pokemons from sessionstorage
        //If it's not in sessionstorage yet, it checks again every 100ms
        let stringOfPokemons = sessionStorage.getItem("pokemons")
        while (stringOfPokemons == null) {
            await new Promise(r => setTimeout(r, 100))
            stringOfPokemons = sessionStorage.getItem("pokemons")
        }
        
        //Creates list of CataloguePokemon from the list read from session storage
        let listOfPokemons:pokemonList[] = JSON.parse(stringOfPokemons)
        this.pokemonCatalogue = listOfPokemons.map( (pokemon):any => {
            return new CataloguePokemon(
                this.getIdByUrl(pokemon.url), 
                pokemon.name, 
                this.isPokemonCaught(pokemon.name))})
    }

}