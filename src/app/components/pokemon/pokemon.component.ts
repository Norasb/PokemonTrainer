import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.css']
})

export class pokemonComponent {
    
    @Input('name') name : string = ''
    @Input('sprite') sprite : string = ''
    @Output() pokemonAction: EventEmitter<string> = new EventEmitter()
    @Output() releaseAction: EventEmitter<string> = new EventEmitter()
    pokemonClicked() {
        this.pokemonAction.emit(this.name);
    }
    releaseClicked() {
        this.releaseAction.emit(this.name);
    }
}