import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "app-nav",
    templateUrl: "nav.component.html",
    styleUrls: ["nav.component.css"]
})

export class NavComponent {

    constructor(private readonly router: Router) {}

    isWhere(): boolean {
        if (this.router.url === "/") {
            return true;
        } else {
            return false
        }
    }

}