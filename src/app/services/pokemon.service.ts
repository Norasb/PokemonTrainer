import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { finalize } from "rxjs";
import { environment } from 'src/environments/environment.development';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, pokemonList, response } from "../models/pokemon";
import { StorageUtil } from '../utils/storage.util';

const { pokeApiUrl, basePokeApiUrl } = environment;

@Injectable({
    providedIn:'root'
})

export class PokemonService{
    
    // http klient and local state variables.
    constructor(private readonly http: HttpClient){}

    private _batch : pokemonList[] = [];
    private _pokemon : Pokemon = new Pokemon(0," ",0,0,[" "],[" "]);
    private _loading : boolean = true;
    private _nextBatch : string = "";
    private _Collection : Pokemon[] = []
    

    // Gets n pokemon objects from api and sets them in session storage where n is the limit set in the pokeApiuRL ?limit = parameter.
    // the method also sets the _batch, and _nextBatch which can be used for pagination.
    public getPokemons() : void{
        this._loading = true;
        this.http.get<response>(pokeApiUrl)
        .pipe(finalize(()=>{
            this._loading = false;
        }))
        .subscribe({
            next: (response:response) =>{
                 this._nextBatch = response.next;
                 this._batch = response.results;
                 sessionStorage.setItem(StorageKeys.Pokemons, JSON.stringify(response.results))
            }
        })
    }
// takes in string to spesific pokemon and sets _pokemon as it.
    private findPokemon(url:string):void{
        this._loading = true;
        this.http.get<Pokemon>(url)
        .pipe(finalize(()=>{
            this._loading = false;
        }))
        .subscribe({
            next: (result : Pokemon) => {
                this._pokemon = result;
            }
        })
    }
// gets a pokemon object from the api and sets it into session storage.
    public addPokemon(url:string):void{
        this._loading = true;
        this.http.get<Pokemon>(url)
        .pipe(finalize(()=>{
            this._loading = false;
        }))
        .subscribe({
            next: (result : Pokemon) => {
                this._Collection.push(result);
                StorageUtil.storageSave<Pokemon[]>(StorageKeys.trainerCollection, this._Collection)
            }
        })
    }

// getter and setter methods
    getBatch() :pokemonList[]{
        return this._batch;
    }
    getLoading() :boolean {
        return this._loading;
    }
    getPokemon() : Pokemon {
        return this._pokemon;
    }
    // gets collection of pokemons, if no pokemons are in storage it will simply return an empty array.
    getPokeCollection(): Pokemon[] {
        let storageCollection : Pokemon[] |undefined = StorageUtil.storageRead<Pokemon[]>(StorageKeys.trainerCollection)
        if(storageCollection!== undefined){
            this._Collection = storageCollection;
        }
        return this._Collection;
    }
    // set pokemon methods, will see if the pokemon object is allready in session storage in which case it will set it in state and return it otherwise it will fetch it from the api before returning it. 
    setPokemonByUrl(url:string) :void {
        this.findPokemon(url)
    }
    setPokemonByName(name:string) :void {
        if (this._Collection.map(x=> x.name).includes(name)) {
            this._pokemon = this._Collection.filter(x=>x.name===name)[0]
        }else{
            this.findPokemon(basePokeApiUrl+name)
        }
    }
    // used to convert the trainer objects list of pokemon names into a list of pokemon objects. these objects will be set into session storage from where they can be utilized.
    setCollection(names : string[]) :void{
        for (const name of names) {
            this.addPokemon(basePokeApiUrl+name)
        }
    }
    // removes pokemon from session storage NOTE that this will not remove it from the trainer api meaning the pokemon-trainer.service.removePokemon method will have to also be called. it is not called here for visability.
    removePokemon(idx : number) : void{
        this._Collection.splice(idx, 1)
        StorageUtil.storageSave<Pokemon[]>(StorageKeys.trainerCollection, this._Collection)
    }
}