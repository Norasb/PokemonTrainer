import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from 'src/app/app-routing.module';

import { AppComponent } from './app.component';
import { ButtonComponent } from './components/button/button.component';
import { CatalogueListing } from './components/catalogue-listing/catalogue-listing.component';
import { PokemonCard } from './components/pokemon-card/pokemon-card.component';
import { LoginformComponent } from './components/loginform/loginform.component';
import { pokemonComponent } from './components/pokemon/pokemon.component';
import { TrainerCollectionComponent } from './components/trainer-collection/trainer-collection.component';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { TrainerParentComponent } from './components/trainer-parent/trainer-parent.component';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { FormsModule } from '@angular/forms';
import { NavComponent } from './components/nav/nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [ //Components
    AppComponent,
    TrainerPage,
    LoginPage,
    CataloguePage,
    ButtonComponent,
    LoginformComponent,
    pokemonComponent,
    TrainerCollectionComponent,
    CatalogueListing,
    PokemonCard,
    TrainerParentComponent,
    NavComponent,
    MenuButtonComponent,
  ],
  imports: [ //Modules
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
