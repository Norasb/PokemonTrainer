import { Component, Output, EventEmitter } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { StorageKeys } from "src/app/enums/storage-keys.enum";
import { PokemonTrainer } from "src/app/models/pokemon-trainer.model";
import { LoginService } from "src/app/services/login.service";
import { PokemonTrainerService } from "src/app/services/pokemon-trainer.service";
import { PokemonService } from "src/app/services/pokemon.service";
import { StorageUtil } from "src/app/utils/storage.util";


@Component({
    selector: 'app-loginform',
    templateUrl: './loginform.component.html',
    styleUrls: ['./loginform.component.css']
})

export class LoginformComponent {

    @Output() login: EventEmitter<void> = new EventEmitter();

    private _pokemonTrainer?: PokemonTrainer;
    public isLoading = false;

    get pokemonTrainer(): PokemonTrainer | undefined {
        return this._pokemonTrainer;
    }

    set pokemonTrainer(pokemonTrainer: PokemonTrainer | undefined) {
        StorageUtil.storageSave<PokemonTrainer>(StorageKeys.PokemonTrainer, pokemonTrainer!);
        this._pokemonTrainer = pokemonTrainer;
    }
    
    constructor(
        private readonly loginService: LoginService, 
        private readonly pokemonTrainerService: PokemonTrainerService, 
        private readonly router: Router,
        private readonly pokemonService: PokemonService) {
        this._pokemonTrainer = StorageUtil.storageRead<PokemonTrainer>(StorageKeys.PokemonTrainer);
    }

    public loginSubmit(loginForm: NgForm): void {

        if (!this._pokemonTrainer) {
            const { username } = loginForm.value;
            this.isLoading = true;
            this.loginService.login(username)
            .subscribe({
                next: (pokemonTrainer: PokemonTrainer) => {
                    this.pokemonService.getPokemons();
                    this.pokemonTrainerService.pokemonTrainer = pokemonTrainer;
                    this.login.emit();
                    this.router.navigateByUrl("/trainer");
                    this.isLoading = false;
                },
                error: () => {
                    this.isLoading = false;
                }
            })
            
        } else {
            alert("You are already logged in, you have to log out first");
        }
    }
}