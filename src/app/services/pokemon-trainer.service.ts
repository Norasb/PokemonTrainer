import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.development';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonTrainer } from '../models/pokemon-trainer.model';
import { StorageUtil } from '../utils/storage.util';

const { apiUsers } = environment;

@Injectable({
    providedIn: 'root'
})



export class PokemonTrainerService {

    // getter and setter methods.
    private _pokemonTrainer?: PokemonTrainer;

    get pokemonTrainer(): PokemonTrainer | undefined {
        return this._pokemonTrainer;
    }

    set pokemonTrainer(pokemonTrainer: PokemonTrainer | undefined) {
        StorageUtil.storageSave<PokemonTrainer>(StorageKeys.PokemonTrainer, pokemonTrainer!);
        this._pokemonTrainer = pokemonTrainer;
    }

    constructor(private readonly http: HttpClient){
        this._pokemonTrainer = StorageUtil.storageRead<PokemonTrainer>(StorageKeys.PokemonTrainer);
    }

    // updates the trainer.pokemon list in both the api and in session storage.
    private updatePokemon(): void{
        const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "x-api-key": "signify"
        })

        this.http.patch<PokemonTrainer>(`${apiUsers}/${this._pokemonTrainer?.id}`, {pokemon : this.pokemonTrainer?.pokemon}, {
          headers
        }).subscribe({
            complete : ()=>{
            }
        })
    }
    // adds pokemon object to trainer in both api and in the session storage.
    public addToPokemons(pokemon: string): void {
        if (this._pokemonTrainer) {
            this._pokemonTrainer.pokemon.push(pokemon);
            this.updatePokemon()
            StorageUtil.storageSave<PokemonTrainer>(StorageKeys.PokemonTrainer, this._pokemonTrainer!);
        }
    }
    // removes a pokemon from trainer in both api and in the session storage.
    public removeFromPokemons(pokemonIndex: number): void {
        if (this._pokemonTrainer) {
            if (this._pokemonTrainer.pokemon.length===1) {
                this._pokemonTrainer.pokemon = []
            }else{
                this._pokemonTrainer.pokemon.splice(pokemonIndex, 1);
            }
            this.updatePokemon()
            StorageUtil.storageSave<PokemonTrainer>(StorageKeys.PokemonTrainer, this._pokemonTrainer!);
        }
    }

}