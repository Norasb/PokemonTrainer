export const environment = {
    production: false,
    apiUsers: "https://statuesque-complex-carrot.glitch.me/trainers",
    pokeApiUrl: "https://pokeapi.co/api/v2/pokemon?limit=20",
    basePokeApiUrl : "https://pokeapi.co/api/v2/pokemon/"
};
