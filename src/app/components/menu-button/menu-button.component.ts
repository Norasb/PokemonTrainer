import { Component, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.css']
})
export class MenuButtonComponent {

  constructor(private readonly router: Router, private readonly loginService: LoginService ) {}

    @Output() logout: EventEmitter<void> = new EventEmitter();

    navigateToPage(page: string) {
        this.router.navigateByUrl(`/${page}`);
    }

    public logoutUser(): void {
        this.loginService.logout();
        this.logout.emit();
        location.reload();
    }

}
