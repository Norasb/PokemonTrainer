export enum StorageKeys {
    PokemonTrainer = "pokemon-trainer",
    trainerCollection = "trainer-collection",
    Pokemons = "pokemons"
}
// this key are used to access variables in session storage.