import { Component, Input, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon";
import { PokemonTrainerService } from "src/app/services/pokemon-trainer.service";
import { PokemonService } from "src/app/services/pokemon.service";


@Component({
    selector: 'app-trainer-collection',
    templateUrl: './trainer-collection.component.html',
    styleUrls: ['./trainer-collection.component.css']
})

export class TrainerCollectionComponent implements OnInit {
    @Input('collection')collection :string[] = []
    constructor(private readonly pokemonService: PokemonService, private readonly pokemonTrainerService : PokemonTrainerService){}

    public handleClicked(value:string) :void {
        this.pokemonService.setPokemonByName(value)
    }
    public handleRelease(value:string) :void{
        this.pokemonService.removePokemon(this.collection.indexOf(value))
        this.pokemonTrainerService.removeFromPokemons(this.collection.indexOf(value))
    }
    
    ngOnInit(): void {
        // get all pokemons from lsit of pokemon names.
        if (this.pokeCollection.length != this.collection.length) {  
            this.pokemonService.setCollection(this.collection)
        } 
        
    }
    get loading() :boolean{
        return this.pokemonService.getLoading();
    }
    get pokeCollection() : Pokemon[] {
        return this.pokemonService.getPokeCollection();
    }
    get selectedPokemon() : Pokemon{
        return this.pokemonService.getPokemon();
    }
}