import { Component, OnInit } from "@angular/core";
import { PokemonTrainer } from "src/app/models/pokemon-trainer.model";
import { PokemonTrainerService } from "src/app/services/pokemon-trainer.service";

@Component({
    selector: 'app-trainer-parent',
    templateUrl: './trainer-parent.component.html',
    styleUrls:['./trainer-parent.component.css']
})

export class TrainerParentComponent implements OnInit {

    constructor(private readonly pokemonTrainerService : PokemonTrainerService){}

    collection :string[] = []

    ngOnInit(): void {
        if (this.pokemonTrainer !== undefined) {
            this.collection = this.pokemonTrainer.pokemon  
        }
    }
    
    get pokemonTrainer():PokemonTrainer | undefined{
        return this.pokemonTrainerService.pokemonTrainer
    }
}