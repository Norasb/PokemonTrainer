import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginPage } from "./pages/login/login.page";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { TrainerPage } from "./pages/trainer/trainer.page";
import { AuthGuard } from "./guards/auth.guard";

const routes : Routes = [
    {
        path: 'trainer',
        component: TrainerPage,
        canActivate: [ AuthGuard ]
    },
    {
        path: '',
        component: LoginPage
    },
    {
        path: 'catalogue',
        component: CataloguePage,
        canActivate: [ AuthGuard ]
    }
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})

export class AppRoutingModule{}