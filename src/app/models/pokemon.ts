export interface pokemonList{
    name: string,
    url: string
}
export interface response{
    count:number,
    next: string,
    previous: string | null,
    results: pokemonList[]
}

export class Pokemon {
    public id:number;
    public name:string;
    public height:number;
    public weight:number;
    public sprites :string[];
    public types : string[];

    constructor(id:number, name:string, height:number,weight:number,sprites:string[], types:string[]){
        this.id = id;
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.sprites = sprites;
        this.types = types;
    }
}

export class CataloguePokemon {
    public id:number;
    public name:string;
    public caught:boolean;

    constructor(id:number, name:string, caught:boolean){
        this.id = id;
        this.name = name;
        this.caught = caught;
    }

}



