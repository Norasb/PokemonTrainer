import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonTrainer } from '../models/pokemon-trainer.model';
import { StorageUtil } from '../utils/storage.util';

const { apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependenciy Injection
  constructor(private readonly http: HttpClient) { }

  
  public login(username: string): Observable<PokemonTrainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((response: PokemonTrainer | undefined) => {
          if (response === undefined) { //user does not exist
            return this.createUser(username);
          }
          return of(response);
        }),
        tap((user: PokemonTrainer) => {
          StorageUtil.storageSave<PokemonTrainer>(StorageKeys.PokemonTrainer, user);
        })
      )
  }

  //Check if user exist
  private checkUsername(username: string): Observable<PokemonTrainer | undefined> {
    return this.http.get<PokemonTrainer[]>(`${apiUsers}?username=${username}`)
      .pipe(
        map((response: PokemonTrainer[]) => response.pop())
      )
  } 


  private createUser(username: string): Observable<PokemonTrainer> {
    const user = {
      username,
      pokemon: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": "signify"
    })

    return this.http.post<PokemonTrainer>(apiUsers, user, {
      headers
    })
  }

  public logout(): boolean {
    StorageUtil.storageRemove(StorageKeys.PokemonTrainer);
    StorageUtil.storageRemove(StorageKeys.Pokemons);
    StorageUtil.storageRemove(StorageKeys.trainerCollection);
    return true;
  }

}
